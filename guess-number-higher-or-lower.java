/* The guess API is defined in the parent class GuessGame.
   @param num, your guess
   @return -1 if my number is lower, 1 if my number is higher, otherwise return 0
      int guess(int num); */

//NOT ACCEPTED using average instead of median too slow
// public class Solution extends GuessGame {
//     public int guessNumber(int n) {
//         if (n == 1) {
//             return 1;
//         };
//
//         int lowest = 1;
//         int highest = n;
//         int num = (highest + lowest) / 2;
//         int response = guess(num);
//
//         while (response != 0) {
//             if (response > 0) {
//                  lowest = num + 1;
//             } else {
//                 highest = num - 1;
//             };
//
//             num = (highest + lowest) / 2;
//             response = guess(num);
//         }
//
//         return num;
//     }
// }


public class Solution extends GuessGame {
    public int guessNumber(int n) {
        int lowest = 1;
        int highest = n;

        while (lowest <= highest) {
            int num = (highest - lowest) / 2 + lowest;
            int response = guess(num);
            if (response == 0) {
                return num;
            } else if (response > 0) {
                 lowest = num + 1;
            } else {
                highest = num - 1;
            };
        }

        return -1;
    }
}
