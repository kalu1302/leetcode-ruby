=begin

Given a binary tree, return all root-to-leaf paths.

For example, given the following binary tree:

   1
 /   \
2     3
 \
  5
All root-to-leaf paths are:

["1->2->5", "1->3"]
Credits:
Special thanks to @jianchao.li.fighter for adding this problem and creating all test cases.

=end

# Definition for a binary tree node.
# class TreeNode
#     attr_accessor :val, :left, :right
#     def initialize(val)
#         @val = val
#         @left, @right = nil, nil
#     end
# end

# @param {TreeNode} root
# @return {String[]}

def binary_tree_paths(tree_root)
  return [] if tree_root.nil?

  answers = []
  stack = []
  stack.push([tree_root, [tree_root.val]])

  until stack.empty?
    node, path = stack.pop

    if node.left.nil? && node.right.nil?
      answers.push(path)
    else
      stack.push([node.right, path + [node.right.val]]) unless node.right.nil?
      stack.push([node.left, path + [node.left.val]]) unless node.left.nil?
    end
  end

  answers.map { |answer| answer.join("->") }
end
